package wahyu.gista.utsalatsekolah

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_data_barang.*
import wahyu.gista.utsalatsekolah.Adapter.ListDataBarang
import wahyu.gista.utsalatsekolah.DBHelper.DBHelper
import wahyu.gista.utsalatsekolah.Model.Barang

class DataBarang: AppCompatActivity() {

    internal lateinit var db: DBHelper
    internal var lstBarang:List<Barang> = ArrayList<Barang>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_barang)

        db = DBHelper(this)

        refreshData()

        //Event
        btnTambah.setOnClickListener {
            val barang = Barang(
                Integer.parseInt(ed_inidbuku.text.toString()),
                ed_judul.text.toString(),
                ed_pengarang.text.toString()
            )
            db.addBarang(barang)
            refreshData()
        }

        btnUpdate.setOnClickListener {
            val barang = Barang(
                Integer.parseInt(ed_inidbuku.text.toString()),
                ed_judul.text.toString(),
                ed_pengarang.text.toString()
            )
            db.updateBarang(barang)
            refreshData()
        }

        btnDelete.setOnClickListener {
            val barang = Barang(
                Integer.parseInt(ed_inidbuku.text.toString()),
                ed_judul.text.toString(),
                ed_pengarang.text.toString()
            )
            db.deleteBarang(barang)
            refreshData()
        }
    }

    private fun refreshData() {
        lstBarang = db.allBarang
        val adapter = ListDataBarang(this@DataBarang,lstBarang,ed_inidbuku,ed_judul,ed_pengarang)
        list_barang.adapter = adapter
    }
}
