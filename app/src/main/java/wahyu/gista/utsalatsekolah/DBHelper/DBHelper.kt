package wahyu.gista.utsalatsekolah.DBHelper

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import wahyu.gista.utsalatsekolah.Model.Barang

class DBHelper(context: Context): SQLiteOpenHelper(context,DARABASE_NAME, null,DATABASE_VER) {

    companion object {
        private val DATABASE_VER = 1
        private val DARABASE_NAME = "utsalatsekolah.db"

        //table buku
        private val TABLE_BARANG="Barang"
        private val COL_ID_BARANG="IdBarang"
        private val COL_NAMA_BARANG="NamaBarang"
        private val COL_JUMLAH_BARANG="JumlahBarang"

    }

    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_TABLE_QUERY1 = ("CREATE TABLE $TABLE_BARANG ($COL_ID_BARANG INTEGER PRIMARY KEY, $COL_NAMA_BARANG TEXT, $COL_JUMLAH_BARANG TEXT)")
        db!!.execSQL(CREATE_TABLE_QUERY1)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_BARANG")
    }


    //crud buku
    val allBarang:List<Barang>
        get() {
            val lstBarang = ArrayList<Barang>()
            val selectQuery = "SELECT * FROM $TABLE_BARANG"
            val db = this.writableDatabase
            val cursor = db.rawQuery(selectQuery,null)
            if(cursor.moveToFirst()){
                do{
                    val barang = Barang()
                    barang.id_barang = cursor.getInt(cursor.getColumnIndex(COL_ID_BARANG))
                    barang.nama_barang = cursor.getString(cursor.getColumnIndex(COL_NAMA_BARANG))
                    barang.jumlah_barang = cursor.getString(cursor.getColumnIndex(COL_JUMLAH_BARANG))

                    lstBarang.add(barang)
                }while(cursor.moveToNext())
            }
            db.close()
            return lstBarang
        }

    fun addBarang(barang: Barang)
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID_BARANG,barang.id_barang)
        values.put(COL_NAMA_BARANG,barang.nama_barang)
        values.put(COL_JUMLAH_BARANG,barang.jumlah_barang)

        db.insert(TABLE_BARANG,null,values)
        db.close()
    }

    fun updateBarang(barang: Barang):Int
    {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(COL_ID_BARANG,barang.id_barang)
        values.put(COL_NAMA_BARANG,barang.nama_barang)
        values.put(COL_JUMLAH_BARANG,barang.jumlah_barang)

        return db.update(TABLE_BARANG,values,"$COL_ID_BARANG=?", arrayOf(barang.id_barang.toString()))

    }

    fun deleteBarang(barang: Barang)
    {
        val db = this.writableDatabase

        db.delete(TABLE_BARANG,"$COL_ID_BARANG=?", arrayOf(barang.id_barang.toString()))
    }

}