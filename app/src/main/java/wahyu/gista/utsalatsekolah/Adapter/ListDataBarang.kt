package wahyu.gista.utsalatsekolah.Adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_list_barang.view.*
import wahyu.gista.utsalatsekolah.Model.Barang
import wahyu.gista.utsalatsekolah.R

class ListDataBarang (internal var activity: Activity,
                      internal var lstBarang: List<Barang>,
                      internal var ed_inidbuku: EditText,
                      internal var ed_judul: EditText,
                      internal var ed_pengarang: EditText
): BaseAdapter() {

    internal var inflater: LayoutInflater

    init {
        inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView: View
        rowView = inflater.inflate(R.layout.activity_list_barang,null)

        rowView.txt_id_barang.text = lstBarang[position].id_barang.toString()
        rowView.txt_nama_barang.text = lstBarang[position].nama_barang.toString()
        rowView.txt_jumlah_barang.text = lstBarang[position].jumlah_barang.toString()

        //event
        rowView.setOnClickListener {
            ed_inidbuku.setText(rowView.txt_id_barang.text.toString())
            ed_judul.setText(rowView.txt_nama_barang.text.toString())
            ed_pengarang.setText(rowView.txt_jumlah_barang.text.toString())
        }
        return rowView
    }
    override fun getItem(position: Int): Any {
        return lstBarang[position]
    }

    override fun getItemId(position: Int): Long {
        return lstBarang[position].id_barang.toLong()
    }

    override fun getCount(): Int {
        return lstBarang.size
    }
}