package wahyu.gista.utsalatsekolah

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_menu_utama.*

class MenuUtama: AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_utama)

        btnBarang.setOnClickListener(this)
        btnKasir.setOnClickListener(this)
    }
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnBarang ->{
                var intentBarang = Intent(this, DataBarang::class.java)
                startActivity(intentBarang)
            }
            R.id.btnKasir ->{
                var intentKasir = Intent(this, Kasir::class.java)
                startActivity(intentKasir)
            }
        }
    }
}